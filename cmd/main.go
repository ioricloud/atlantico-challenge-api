package main

import (
	"atlantico-challenge-api/pkg/db"
	"atlantico-challenge-api/pkg/handlers"
	"atlantico-challenge-api/pkg/middleware"
	"log"
	"net/http"
	"os"

	_ "atlantico-challenge-api/docs"

	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	httpSwagger "github.com/swaggo/http-swagger"
)

// @title Api-go Atlantico
// @version 1.0
// @description Api feita em go para o Desafio Job Position Inst. Atlantico
// @termsOfService http://swagger.io/terms/
// @host localhost:8000
// @BasePath /
func main() {
	DB := db.Init()
	h := handlers.NewDB(DB)
	r := mux.NewRouter()
	r.Use(middleware.LoggingMiddleware)

	log.Println("Api está rodando e todos os Endpoints apontados")
	// Todos os endpoints
	r.HandleFunc("/v1/project", h.GetProjects).Methods(http.MethodGet)
	r.HandleFunc("/v1/project/{id}", h.GetProject).Methods(http.MethodGet)
	r.HandleFunc("/v1/project", h.AddProject).Methods(http.MethodPost)
	r.HandleFunc("/v1/project/{id}", h.UpdateProject).Methods(http.MethodPut)
	r.HandleFunc("/v1/project/{id}", h.DeleteProject).Methods(http.MethodDelete)

	// Endpoint para Metricas do Prometeus e Swagger
	r.Handle("/metrics", promhttp.Handler())
	r.PathPrefix("/docs/").Handler(httpSwagger.WrapHandler)

	http.ListenAndServe(":"+os.Getenv("APIPORT"), r)
}
