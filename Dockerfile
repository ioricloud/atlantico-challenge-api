# Global ARGs
ARG APIPORT

#First Stage
FROM golang:alpine as build

RUN apk update && apk add --no-cache git

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -o main cmd/main.go

#Second Stage
FROM alpine:latest

RUN apk update && apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=build /app/main .

EXPOSE ${APIPORT}

CMD [ "./main" ]