provider "random" {}

resource "random_integer" "porta" {
  min = 8000
  max = 8050
}

locals {
  host_lb_port = (var.k3d_host_lb_port != "" ? var.k3d_host_lb_port : random_integer.porta.result)
}

resource "null_resource" "cluster" {
  for_each = toset(var.k3d_cluster_name)
  triggers = {
    "agent_count"  = var.agent_count
    "server_count" = var.server_count
    "ip"           = var.k3d_cluster_ip
    "port"         = var.k3d_cluster_port
    "k3s_version"  = var.k3s_version
  }
  provisioner "local-exec" {
    command = "k3d cluster create ${each.key} -a ${var.agent_count} -s ${var.server_count} --api-port ${var.k3d_cluster_ip}:${var.k3d_cluster_port} -p ${local.host_lb_port}:${var.k3d_cluster_lb_port}@loadbalancer --image rancher/k3s:${var.k3s_version}"
  }
}

resource "null_resource" "cluster_delete" {
  for_each = toset(var.k3d_cluster_name)
  provisioner "local-exec" {
    command = "k3d cluster delete ${each.key}"
    when    = destroy
  }
}

resource "null_resource" "build-api-go" {
    for_each = toset(var.bdimage)
    provisioner "local-exec" {
      command = "docker build -t ${each.key} ."
    }

    depends_on = [
        null_resource.cluster
    ]
}

resource "null_resource" "cluster_access" {
  for_each = toset(var.k3d_cluster_name)
  provisioner "local-exec" {
    command = "kubectl config set-context ${each.key}"
  }

  depends_on = [
    null_resource.build-api-go
  ]
}

resource "null_resource" "create_namespace" {
    provisioner "local-exec" {
        command = "kubectl apply -f manifests/namespace.yaml"
    }

    depends_on = [
        null_resource.cluster_access
    ]
}

resource "null_resource" "create_postgres" {
  provisioner "local-exec" {
    command = "kubectl apply -f manifests/pg-configuration.yaml"
  }

  depends_on = [
    null_resource.create_namespace
  ]
}

resource "null_resource" "create_api-go" {
  provisioner "local-exec" {
    command = "kubectl apply -f manifests/api-configuration.yaml"
  }

  depends_on = [
    null_resource.create_postgres
  ]
}
