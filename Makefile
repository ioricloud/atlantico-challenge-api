START=$(pwd)
USERHUB=ioricloud
APPIMAGE=app-go
TAG=test
BUILD=$(USERHUB)/$(APPIMAGE):$(TAG)
VAR-PORTAPI=8000
VAR-HOSTDB=postgres
VAR-USERDB=postgres
VAR-PASSWORDDB=postgres
VAR-NAMEDB=project
VAR-PORTDB=5432
VAR-NS=application

buildimage:
		docker build -t $(USERHUB)/$(APPIMAGE):$(TAG) .

pushimage:
		docker push $(USERHUB)/$(APPIMAGE):$(TAG)
		docker image prune -f
		docker rmi $(USERHUB)/$(APPIMAGE):$(TAG)

composeup:
		sed -i 's/usuario/$(USERHUB)/g' docker-compose.yml
		sed -i 's/application/$(APPIMAGE)/g' docker-compose.yml
		sed -i 's/tag/$(TAG)/g' docker-compose.yml
		docker-compose up -d

composedown:
		docker-compose down
		sed -i 's/$(USERHUB)/usuario/g' docker-compose.yml
		sed -i 's/$(APPIMAGE)/application/g' docker-compose.yml
		sed -i 's/$(TAG)/tag/g' docker-compose.yml
		docker rmi $(USERHUB)/$(APPIMAGE):$(TAG)

terraforminit:
		terraform init
		terraform plan

terraformup:
		echo 'Este comando vai aplicar a infraestrutura automaticamente!'
		sed -i 's/usuario/$(USERHUB)/g' manifests/*.yaml
		sed -i 's/appimage/$(APPIMAGE)/g' manifests/*.yaml
		sed -i 's/tag/$(TAG)/g' manifests/*.yaml
		sed -i 's/portaapi/$(VAR-PORTAPI)/g' manifests/*.yaml
		sed -i 's/hostdatabase/$(VAR-HOSTDB)/g' manifests/*.yaml
		sed -i 's/userdatabase/$(VAR-USERDB)/g' manifests/*.yaml
		sed -i 's/passdatabase/$(VAR-PASSWORDDB)/g' manifests/*.yaml
		sed -i 's/namedatabase/$(VAR-NAMEDB)/g' manifests/*.yaml
		sed -i 's/portdatabase/$(VAR-PORTDB)/g' manifests/*.yaml
		sed -i 's/k8s-namespace/$(VAR-NS)/g' manifests/*.yaml
		terraform apply -auto-approve

terraformdown:
		echo 'Este comando vai destruir a infraestrutura automaticamente!'
		terraform destroy -auto-approve
		sed -i 's/$(USERHUB)/usuario/g' manifests/*.yaml
		sed -i 's/$(APPIMAGE)/appimage/g' manifests/*.yaml
		sed -i 's/$(TAG)/tag/g' manifests/*.yaml
		sed -i 's/$(VAR-PORTAPI)/portaapi/g' manifests/*.yaml
		sed -i 's/$(VAR-HOSTDB)/hostdatabase/g' manifests/*.yaml
		sed -i 's/$(VAR-USERDB)/userdatabase/g' manifests/*.yaml
		sed -i 's/$(VAR-PASSWORDDB)/passdatabase/g' manifests/*.yaml
		sed -i 's/$(VAR-NAMEDB)/namedatabase/g' manifests/*.yaml
		sed -i 's/$(VAR-PORTDB)/portdatabase/g' manifests/*.yaml
		sed -i 's/$(VAR-NS)/k8s-namespace/g' manifests/*.yaml