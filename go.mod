module atlantico-challenge-api

go 1.15

require (
	github.com/go-openapi/loads v0.21.1 // indirect
	github.com/go-openapi/runtime v0.23.0 // indirect
	github.com/go-openapi/strfmt v0.21.2 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/go-swagger/go-swagger v0.29.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/prometheus/client_golang v1.12.1
	github.com/spf13/afero v1.8.1 // indirect
	github.com/swaggo/http-swagger v1.2.5
	github.com/swaggo/swag v1.7.9
	github.com/yvasiyarov/swagger v0.0.0-20180817222219-39eb437316e9 // indirect
	go.mongodb.org/mongo-driver v1.8.3 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/tools v0.1.9 // indirect
	gopkg.in/ini.v1 v1.66.4 // indirect
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.5
)
