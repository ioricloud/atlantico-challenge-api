# Challenge SRE Atlantico 

### Introdução

Este Readme é a documentação de um desafio SRE do Instituto Atlantico.

Projeto de um api Go nos moldes da filosofia 12 Factors, com arquivo *Dockerfile* e *Docker Compose*.

Logo abaixo, informações sobre endpoints, dockerfile e docker-compose.

---

### API

Api tem que ser setada as variaveis para que funcione, precisa ser setada as mesmas variaveis que sera setada se for usada como container. Logo abaixo, existe estas variaveis para que possa ser colocada em um arquivo .env, nao esqueça se for usar api para ser gerada pelo build, de que é necessario ter o arquivo.

Api tem todos os endpoints apontado para `/v1/project` somente modificando para criaçao, atualização e remoção usando id de cada item.

> *Endpoint GET /v1/project*
Este endpoint tras todos items da tabela project.

> *Endpoint GET /v1/project/{id}*
Este endpoint tras o item referente ao id da tabela project.

> *Endpoint POST /v1/project*
Este endpoint cria um novo item na tabela project.

> *Endpoint PUT /v1/project/{id}*
Este endpoint atualizar um item referente ao id da tabela project.

> *Endpoint DELETE /v1/project/{id}*
Este endpoint deleta um item referente ao id da tabela project.

> *Endpoint GET /metrics*
Este endpoint é para pegar todas as metricas da mesma api, usando Prometheus.

> *Endpoint GET /docs/*
Este endpoint é para verificar todas as rotas documentadas via Swagger.

---

### Dockerfile

Dockerfile pedido, efetuei a criação deste usando multistage. Deixei da melhor forma o mais compacta possivel.

Primeira parte do dockerfile é somente para geração do binario em linux e a segunda parte é somente para executar o binario.

*Obs: ao fazer clone deste repo, necessario criar o arquivo .env, para geração da imagem. Este é um arguimento dentro do Dockerfile*

A imagem ao ser executada para o container necessita ser setada as variaveis de ambiente.

Variaveis:

> APIPORT - variavel que modifica qual a porta padrão para acessar a api ao ser deployada.

> DBHOST - variavel para configuração do endereço host da instancia do banco de dados.

> DBUSER - variavel para configuração de usuário desta instancia de banco de dados.

> DBPASSOWRD - variavel para configuração de senha de acesso desta instancia de banco de dados.

> DBNAME - variavel para configuração do nome do banco de dados da instancia.

> DBPORT - variavel para configuração de porta da instancia de banco de dados.

---

### Docker-Compose

Neste docker-compose foi adicionado dois services, api que é sobre a api, no qual foi gerado a imagem e feita o upload dela no docker hub, e o outro service é postgres para persistência dos dados.

Lembrando que temos duas opções para configurações de variaveis de ambiente para o serviço da API dentro do compose, ou usamos env-file ou declaramos as variaveis usando environment. Enfatizar que no serviço do Postgres, ja setei uma variavel padrao, se caso mude, deve ser mudada no serviço da API.

A imagem do serviço API está usando meu docker-hub pessoal.

> image: usuario/app:tag

Na linha do serviço API está escrito assim, coloquei agnostico para que qualquer pessoa possa testar usando seu usuario do docker-hub. Adicionei um `Makefile` para que pudesse automatizar de forma rapida.

No docker-compose tem uma nova variavel chamada `EXTERNALPORT` para que fosse maleavel para testar usando outra porta do Host.

So reiterando, no docker-compose.yml está setado para se usar o env-file, de quebra, terá que criar um .env na raiz do projeto e adicionar as variaveis informadas, ou se desejar, modificar para para que fique as variaveis diretamente no compose.

---

### Makefile 

A criação do Makefile foi para automatizar todo o processo de teste. Neste arquivo será necessario declarar as variaveis para que possar automatizar todo o processo de build, push, compose up e compose down.

As variaveis são:
USERHUB
APPIMAGE
TAG

Antes de rodar o make, favor, declare as variaveis, as tres variaveis estão relacionadas ao docker-hub.

Os comandos a seguir para automatização são:

> make buildimage

Comando gera uma imagem atrelada a um usuario do docker-hub, com nome da imagem e com a tag que desejar.

> make pushimage

Comando envia a imagem ao seu hub, dar um prune em imagens sem nome e tags e por ultimo tira a imagem da vm.
Usei esta tatica, para comprovar que a imagem vai ser baixada do hub, quando for usado o comando `make composeup`.

> make composeup

Comando irá subir aplicação juntamente com serviço Postgres, que é onde será persistido os dados.
Outro fato deste comando é que ele irá usar o SED para mudar a linha do compose `image: usuario/app:tag` para os nomes que foram escolhidos nas variaveis do Makefile.

> make composedown

Comando irá fazer o reverso do comando `make composeup`, irá descer o projeto do compose, mudará a linha do compose image para versão anterior `image: usuario/app:tag`.

> make terraforminit

Comando irá inicializar o projeto via terraform, irá dar um init e um plan. Lembrando que este plus, fiz usando `K3D`.

> make terraformup

Comando irá subir tudo, criará um cluster no k3d, push da imagem docker, criará um namespace, criará um projeto de banco de dados Postgresql para aplicação, criará o projeto da api. Projeto contem configmap, deployment, service e ingress.

> make terraformdown

Comando irá destruir tudo criado no comando anterior.

---

### Metrics

Adicionei as libs para metricas usando Prometheus.

Este topico é somente informação.

---

### Swagger

Adicionado informações de todos os endpoints usando swag-cmd e notações de Swagger no arquivo main.go e nos endpoints no arquivo project.go