package handlers

import (
	"atlantico-challenge-api/pkg/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	_ "atlantico-challenge-api/docs"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func NewDB(db *gorm.DB) handler {
	return handler{db}
}

// GetProjects godoc
// @Summary Get API Go Version
// @Description Get GetProjects Api Go Version
// @Tags Project
// @Accept json
// @Produce json
// @Success 200 {object} string
// @Router /v1/project [get]
func (h handler) GetProjects(w http.ResponseWriter, r *http.Request) {
	var project []models.Project

	if rst := h.DB.Find(&project); rst.Error != nil {
		fmt.Println(rst.Error)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(project)
}

// GetProject godoc
// @Summary Get Unit API Go Version
// @Description Get GetProject Unit Api Go Version
// @ID get-project
// @Tags Project
// @Accept json
// @Produce json
// @Param id path string true "Projeto Atualizado"
// @Success 200 {object} string
// @Router /v1/project/{id} [get]
func (h handler) GetProject(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	var project models.Project

	if rst := h.DB.First(&project, id); rst.Error != nil {
		fmt.Println(rst.Error)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(project)
}

// AddProject godoc
// @Summary Add API Go Version
// @Description Add AddProject Api Go Version
// @ID add-project
// @Tags Project
// @Accept json
// @Produce json
// @Param data body models.Project true "Projeto Criado"
// @Success 201 {object} string
// @Router /v1/project [post]
func (h handler) AddProject(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var project models.Project
	json.Unmarshal(body, &project)

	if rst := h.DB.Create(&project); rst.Error != nil {
		fmt.Println(rst.Error)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode("Projeto Criado")
}

// UpdateProject godoc
// @Summary Update API Go Version
// @Description Update UpdateProject Api Go Version
// @ID put-project
// @Tags Project
// @Accept json
// @Produce json
// @Param id path string, data body models.Project true "Projeto Atualizado"
// @Success 200 {object} string
// @Router /v1/project/{id} [put]
func (h handler) UpdateProject(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var uProject models.Project
	json.Unmarshal(body, &uProject)

	var project models.Project

	if rst := h.DB.First(&project, id); rst.Error != nil {
		fmt.Println(rst.Error)
	}

	project.Name = uProject.Name
	project.Description = uProject.Description
	project.Version = uProject.Version

	h.DB.Save(&project)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("Projeto Foi Atualizado")
}

// DeleteProject godoc
// @Summary Delete API Go Version
// @Description Delete DeleteProject Api Go Version
// @ID delete-project
// @Tags Project
// @Accept json
// @Produce json
// @Param id path string true "Projeto Atualizado"
// @Success 200 {object} string
// @Router /v1/project/{id} [delete]
func (h handler) DeleteProject(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	var project models.Project

	if rst := h.DB.First(&project, id); rst.Error != nil {
		fmt.Println(rst.Error)
	}

	h.DB.Delete(&project)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("Projeto Foi Deletado")
}
