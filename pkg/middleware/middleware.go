package middleware

import (
	"log"
	"net/http"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.Proto, r.URL, r.ContentLength, r.Referer(), r.UserAgent())
		next.ServeHTTP(w, r)
	})
}
