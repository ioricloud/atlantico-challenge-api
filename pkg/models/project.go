package models

import "time"

type Project struct {
	Id          int       `json:"id" gorm:"primaryKey"`
	Name        string    `json:"name"`
	DisplayName string    `json:"displayname"`
	Description string    `json:"description"`
	Version     string    `json:"version"`
	Create_Time time.Time `json:"create_time" gorm:"type:timestamp;default:current_timestamp"`
	Update_Time time.Time `json:"update_time" gorm:"type:timestamp;default:current_timestamp"`
}
