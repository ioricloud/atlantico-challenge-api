variable "k3s_version" {
  default = "latest"
}

variable "k3d_cluster_name" {
  default = ["atlantico"]
}

variable "k3d_cluster_port" {
  default = 6443
}

variable "k3d_cluster_ip" {
  default = "0.0.0.0"
}

variable "k3d_host_lb_port" {
  default = null
}

variable "k3d_cluster_lb_port" {
  default = 80
}

variable "server_count" {
  default = 3
}

variable "agent_count" {
  default = 3
}

variable "application" {
  default = ["nginx"]
}

variable "namespace" {
  default = ["application"]
}

variable "bdimage" {
  default = ["ioricloud/app-go:test"]
}